Visualogyx will enable the counting of similar objects in industrial and 
logistics scenarios using consumer level smartphones and tablets running 
Android or iOS. It will enable the association of counts with human resources 
through facial detection.