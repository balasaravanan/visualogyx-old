cmake_minimum_required ( VERSION 3.3.2 )
project ( IndustrialVisionEngine )

find_package ( OpenCV REQUIRED )

include ( GenerateExportHeader)
add_library ( IndustrialVisionEngine SHARED hough_circle_finder.cpp )
target_link_libraries ( IndustrialVisionEngine ${OpenCV_LIBS} )
GENERATE_EXPORT_HEADER ( IndustrialVisionEngine 
				BASE_NAME IndustrialVisionEngine
				EXPORT_MACRO_NAME IndustrialVisionEngineExport 
				EXPORT_FILE_NAME IndustrialVisionEngineExport.h )