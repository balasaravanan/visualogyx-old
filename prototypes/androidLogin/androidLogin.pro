#-------------------------------------------------
#
# Project created by QtCreator 2016-04-20T20:13:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = androidLogin
TEMPLATE = app


SOURCES += main.cpp\
        loginwidget.cpp

HEADERS  += loginwidget.h

FORMS    += loginwidget.ui
