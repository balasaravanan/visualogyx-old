#include "loginwidget.h"
#include "ui_loginwidget.h"

loginwidget::loginwidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::loginwidget)
{
    ui->setupUi(this);
}

loginwidget::~loginwidget()
{
    delete ui;
}

void loginwidget::setWidgetSize(QSize size)
{
    this->resize(size);
    ui->stackedWidget->resize(size);
}

void loginwidget::on_signUpButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}
