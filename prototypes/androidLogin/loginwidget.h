#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>

namespace Ui {
class loginwidget;
}

class loginwidget : public QWidget
{
    Q_OBJECT

public:
    explicit loginwidget(QWidget *parent = 0);
    ~loginwidget();

    void setWidgetSize(QSize size);

private slots:
    void on_signUpButton_clicked();

private:
    Ui::loginwidget *ui;
};

#endif // LOGINWIDGET_H
