#ifndef IMG_PROC_H
#define IMG_PROC_H

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdexcept>

using namespace cv;

//Gray scale, gaussian blur, hough circles
int process_img_01(const string &file_name, string &dest_file_name, int &count);

//Gray scale, canny edge detect, hough circles
int process_img_02(const string &file_name, string &dest_file_name, int &count);

//Gray scale, canny edge detect, hough circles, distance transform for partial circles
int process_img_03(const string &file_name, string &dest_file_name, int &count);

#endif // IMG_PROC_H
