#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utilityfunctions.h"
#include "img_proc.h"

#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Visualogyx prototype");

    //graphics view
    width = ui->graphicsView->geometry().width();
    height = ui->graphicsView->geometry().height();

    originalScene = new QGraphicsScene(QRectF(0,0,width,height),0) ;
    processedScene = new QGraphicsScene(QRectF(0,0,width,height),0) ;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_LoadImageButton_clicked()
{
    originalScene->clear();
    processedScene->clear();
    ui->graphicsView->items().clear();
    ui->ProcessedGraphicsView->items().clear();
    QString imgFileName = QFileDialog::getOpenFileName(
                this,
                tr("Select ImageFile"),
                last_seen_path,
                "Image file (*.jpg *.png *.bmp *.JPEG)");

    QFileInfo current_file_info(imgFileName);
    last_seen_path = current_file_info.dir().path();

    copyImage(imgFileName,in_file_path);
    DisplayOnCanvas(originalScene  ,imgFileName, ui->graphicsView);
}

void MainWindow::DisplayOnCanvas(QGraphicsScene *scene, QString imgFileName, QGraphicsView *view)
{
    QPixmap pixmap = QPixmap(imgFileName);
    QGraphicsPixmapItem *imgitem = scene->addPixmap(pixmap.scaled(QSize((int)scene->width(), (int)scene->height()),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    //to display at the centre of the view

    QPixmap pix = pixmap.scaled(QSize((int)scene->width(), (int)scene->height()),Qt::KeepAspectRatio, Qt::SmoothTransformation);
    int w_scaled_pic = pix.width();
    int h_scaled_pic = pix.height();

    int new_origin_x = (width - w_scaled_pic)/2;
    int new_origin_y = (height - h_scaled_pic)/2;
    // set the bg pos
    imgitem->setPos(new_origin_x,new_origin_y);
    imgitem->setZValue(-5);                       // Z value is set so that the characters always appear infront of the background

    //display the scene
    view->setScene(scene);
    view->fitInView(QRectF(0, 0, width, height),Qt::KeepAspectRatio);

    view->show();
}





void MainWindow::on_ProcessImageButton_clicked()
{
    qDebug() << "In process image ... ";
    string dest_file_name = "";
    int count = 0;

    try
    {
        if(ui->GaussHough->isChecked())
        {
            if( process_img_01(in_file_path.toStdString(), dest_file_name, count) == 0 )
            {
                if(dest_file_name != "")
                {
                    DisplayOnCanvas(processedScene, QString(dest_file_name.c_str()), ui->ProcessedGraphicsView);
                }
            }
            else
            {
                qDebug() << "Image processing failed";
            }
        }
        else if(ui->CannyHough->isChecked())
        {
            if( process_img_02(in_file_path.toStdString(), dest_file_name, count) == 0 )
            {
                if(dest_file_name != "")
                {
                    DisplayOnCanvas(processedScene, QString(dest_file_name.c_str()), ui->ProcessedGraphicsView);
                }
            }
            else
            {
                qDebug() << "Image processing failed";
            }
        }
//        else if(ui->CannyHoughDT->isChecked())
//        {
//            if( process_img_03(in_file_path.toStdString(), dest_file_name, count) == 0 )
//            {
//                if(dest_file_name != "")
//                {
//                    DisplayOnCanvas(processedScene, QString(dest_file_name.c_str()), ui->ProcessedGraphicsView);
//                }
//            }
//            else
//            {
//                qDebug() << "Image processing failed";
//            }
//        }
    }
    catch (std::exception &e)
    {
        qDebug() << e.what();
    }
    QString count_string = QString::number(count);
    ui->circlesCountLabel->setText(count_string);

}

void MainWindow::on_GaussHough_clicked()
{
    processedScene->clear();
    ui->ProcessedGraphicsView->items().clear();
}

void MainWindow::on_CannyHough_clicked()
{
    processedScene->clear();
    ui->ProcessedGraphicsView->items().clear();
}

void MainWindow::on_CannyHoughDT_clicked()
{
    processedScene->clear();
    ui->ProcessedGraphicsView->items().clear();
}
