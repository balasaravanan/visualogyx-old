#-------------------------------------------------
#
# Project created by QtCreator 2016-04-06T20:39:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vlapp
TEMPLATE = app

INCLUDEPATH += H:\CognitiveMachines\OpenCV\opencv_2_4_10\include
LIBPATH += H:\CognitiveMachines\OpenCV\opencv_2_4_10\x86\vc10\lib
LIBS +=  opencv_core2410.lib opencv_imgproc2410.lib opencv_highgui2410.lib

SOURCES += main.cpp\
        mainwindow.cpp \
    img_proc.cpp

HEADERS  += mainwindow.h \
    utilityfunctions.h \
    img_proc.h

FORMS    += mainwindow.ui
