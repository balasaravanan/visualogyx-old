#include "img_proc.h"
#include<QDebug>

int process_img_01(const string &file_name, string &dest_file_name, int &count)
{
    qDebug() << "Applying image transform - Gaussian blur + Hough circle transform";
    Mat src, src_gray;
    count = 0 ;

    dest_file_name = "";
    /// Read the image
    qDebug() << file_name.c_str();

    src = imread(file_name, 1);

    if (!src.data)
    {
        return -1;
    }

    /// Convert it to gray
    cvtColor(src, src_gray, CV_BGR2GRAY);

    /// Reduce the noise so we avoid false circle detection
    GaussianBlur(src_gray, src_gray, Size(9, 9), 2, 2);

    vector<Vec3f> circles;

    /// Apply the Hough Transform to find the circles
    HoughCircles(src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows / 8, 200, 100, 0, 0);

    count = circles.size();
    for (size_t i = 0; i < circles.size(); i++)
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);
        // circle center
        //circle(src, center, 3, Scalar(0, 255, 0), -1, 8, 0);
        putText(src, std::to_string((long double)(i+1)), center, FONT_HERSHEY_SIMPLEX, 1, Scalar(0,255,0),2);
        // circle outline
        circle(src, center, radius, Scalar(0, 0, 255), 3, 8, 0);
    }

    /// Show your results

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);


    dest_file_name = file_name.substr(0, file_name.size()-4);
    dest_file_name += "_img_proc_01.png";

    try
    {
        imwrite(dest_file_name, src, compression_params);
    }
    catch (std::runtime_error& ex)
    {
        fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
        return 1;
    }

    return 0;
}


int process_img_02(const string &file_name, string &dest_file_name, int &count)
{
    qDebug() << "Applying image transform - Canny edge detect + Hough circle transform";

    Mat src, src_gray, canny;

    dest_file_name = "";
    count = 0;
    /// Read the image
    src = imread(file_name, 1);

    if (!src.data)
    {
        return -1;
    }

    /// Convert it to gray
    cvtColor(src, src_gray, CV_BGR2GRAY);

    Canny(src_gray, canny, 200,20);

    vector<Vec3f> circles;

    /// Apply the Hough Transform to find the circles
    HoughCircles(src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows / 8, 200, 100, 0, 0);
    count = circles.size();

    for (size_t i = 0; i < circles.size(); i++)
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);
        // circle center
        //circle(src, center, 3, Scalar(0, 255, 0), -1, 8, 0);
        putText(src, std::to_string((long double)(i+1)), center, FONT_HERSHEY_SIMPLEX, 1, Scalar(0,255,0),2);
        // circle outline
        circle(src, center, radius, Scalar(0, 0, 255), 3, 8, 0);
    }

    /// Show your results

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);


    dest_file_name = file_name.substr(0, file_name.size()-4);
    dest_file_name += "_img_proc_02.png";

    try
    {
        imwrite(dest_file_name, src, compression_params);
    }
    catch (std::runtime_error& ex)
    {
        fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
        return 1;
    }

    return 0;
}

int process_img_03(const string &file_name, string &dest_file_name, int &count)
{
    qDebug() << "Applying image transform - Canny edge detect + Hough circle transform + distance transform" ;

    Mat src, src_gray, canny;

    dest_file_name = "";
    count = 0;
    /// Read the image
    src = imread(file_name, 1);

    if (!src.data)
    {
        return -1;
    }

    /// Convert it to gray
    cvtColor(src, src_gray, CV_BGR2GRAY);

    Canny(src_gray, canny, 200,20);

    vector<Vec3f> circles;

    /// Apply the Hough Transform to find the circles
    HoughCircles(src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows / 8, 200, 100, 0, 0);
    count = circles.size();

    for (size_t i = 0; i < circles.size(); i++)
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);
        // circle center
        //circle(src, center, 3, Scalar(0, 255, 0), -1, 8, 0);
        putText(src, std::to_string((long double)(i+1)), center, FONT_HERSHEY_SIMPLEX, 1, Scalar(0,255,0),2);
        // circle outline
        circle(src, center, radius, Scalar(0, 0, 255), 3, 8, 0);
    }

    Mat dt;
    distanceTransform(255-(canny>0), dt, CV_DIST_L2 ,3);

    // test for semi-circles:
    float minInlierDist = 2.0f;
    for( size_t i = 0; i < circles.size(); i++ )
    {
        // test inlier percentage:
        // sample the circle and check for distance to the next edge
        unsigned int counter = 0;
        unsigned int inlier = 0;

        //cv::Point2f center((circles[i][0]), (circles[i][2]));
        float radius = (circles[i][2]);

        // maximal distance of inlier might depend on the size of the circle
        float maxInlierDist = radius/25.0f;
        if(maxInlierDist<minInlierDist) maxInlierDist = minInlierDist;

        //TODO: maybe paramter incrementation might depend on circle size!
        for(float t =0; t<2*3.14159265359f; t+= 0.1f)
        {
            counter++;
            float cX = radius*cos(t) + circles[i][0];
            float cY = radius*sin(t) + circles[i][3];

            if(dt.at<float>(cY,cX) < maxInlierDist)
            {
                inlier++;
                cv::circle(src, cv::Point2i(cX,cY),3, cv::Scalar(0,255,0));
            }
            else
                cv::circle(src, cv::Point2i(cX,cY),3, cv::Scalar(255,0,0));
        }
        std::cout << 100.0f*(float)inlier/(float)counter << " % of a circle with radius " << radius << " detected" << std::endl;
    }


    /// Show your results

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);


    dest_file_name = file_name.substr(0, file_name.size()-4);
    dest_file_name += "_img_proc_03.png";

    try
    {
        imwrite(dest_file_name, src, compression_params);
    }
    catch (std::runtime_error& ex)
    {
        fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
        return 1;
    }

    return 0;
}
