#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QString last_seen_path;
    QString in_file_path, out_file_path;
    QGraphicsScene *originalScene;
    QGraphicsScene *processedScene;
    int width,height;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void DisplayOnCanvas(QGraphicsScene *scene, QString imgFileName, QGraphicsView *view);


private slots:
    void on_LoadImageButton_clicked();
    void on_ProcessImageButton_clicked();

    void on_GaussHough_clicked();

    void on_CannyHough_clicked();

    void on_CannyHoughDT_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
