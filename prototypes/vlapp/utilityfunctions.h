#ifndef UTILITYFUNCTIONS_H
#define UTILITYFUNCTIONS_H

#include <QSize>
#include <QImage>
#include <QString>
#include <QFileInfo>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QLabel>
#include <iostream>
#include <QCoreApplication>

using namespace std;

inline QString appendPath(const QString& path1, const QString& path2)
{
    return QDir::cleanPath(path1 + QDir::separator() + path2);
}

inline bool copyImage(const QString &file_name, QString &out_file_name)
{
    QFileInfo source_file(file_name);
    if(source_file.isFile() != true)
        return false;

    QString appPath = QCoreApplication::applicationDirPath();
    QDir dir(appPath);
    QDir imgDir(appPath + "/" + "Images");

    if(!imgDir.exists())
    {
        dir.mkdir("Images");

        QString f_name = source_file.fileName();
        out_file_name = appendPath((appPath + "/" + "Images"),f_name);

        return QFile::copy(file_name, out_file_name);
    }
    else
    {

        QString f_name = source_file.fileName();
        out_file_name = appendPath((appPath + "/" + "Images"),f_name);

        return QFile::copy(file_name, out_file_name);
    }
}


#endif // UTILITYFUNCTIONS_H

