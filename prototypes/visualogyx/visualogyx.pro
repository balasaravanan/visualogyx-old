#-------------------------------------------------
#
# Project created by QtCreator 2016-04-21T14:35:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = visualogyx
TEMPLATE = app


SOURCES += main.cpp\
        visualogyxapp.cpp

HEADERS  += visualogyxapp.h

FORMS    += visualogyxapp.ui

CONFIG += mobility
MOBILITY =

OTHER_FILES += \
android/AndroidManifest.xml
android/res/drawable-hdpi/icon.png
android/res/drawable-ldpi/icon.png
android/res/drawable-mdpi/icon.png
android/res/values/libs.xml
android/res/values/strings.xml
android/src/eu/licentia/necessitas/industrius/QtActivity.java
android/src/eu/licentia/necessitas/industrius/QtApplication.java
android/src/eu/licentia/necessitas/industrius/QtLayout.java
android/src/eu/licentia/necessitas/industrius/QtSurface.java
android/src/eu/licentia/necessitas/ministro/IMinistro.aidl
android/src/eu/licentia/necessitas/ministro/IMinistroCallback.aidl
android/src/eu/licentia/necessitas/mobile/QtAndroidContacts.java
android/src/eu/licentia/necessitas/mobile/QtCamera.java
android/src/eu/licentia/necessitas/mobile/QtFeedback.java
android/src/eu/licentia/necessitas/mobile/QtLocation.java
android/src/eu/licentia/necessitas/mobile/QtMediaPlayer.java
android/src/eu/licentia/necessitas/mobile/QtSensors.java
android/src/eu/licentia/necessitas/mobile/QtSystemInfo.java

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

