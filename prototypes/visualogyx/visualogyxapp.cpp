#include "visualogyxapp.h"
#include "ui_visualogyxapp.h"

#include <QListWidgetItem>

visualogyxApp::visualogyxApp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::visualogyxApp)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
}

visualogyxApp::~visualogyxApp()
{
    delete ui;
}

void visualogyxApp::setWidgetSize(QSize size)
{
    this->resize(size);
}

//--------------------------------------------------------------------------------------------------
void visualogyxApp::on_loginButton_clicked()
{
    QString emailId  = ui->emailInput->text();
    //QString password = ui->passwordInput->text();

    QString phone = ui->phoneInput->text();
    QString password2 = ui->passwordInput_2->text();

    if((emailId == "admin" && password2 == "password")||(phone == "12345" && password2 == "password"))
    {
        ui->stackedWidget->setCurrentIndex(1);
    }

}

//--------------------------------------------------------------------------------------------------
void visualogyxApp::on_galleryButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void visualogyxApp::on_toggleCameraButton_toggled(bool checked)
{
    if (checked == true)
    {
        //set front camera
    }

    else if(checked == false)
    {
        //set rear camera
    }
}

void visualogyxApp::on_captureButton_clicked()
{
    //take the picture
}

//--------------------------------------------------------------------------------------------------
void visualogyxApp::addToGallery(QPixmap pixmapImage , QString text )
{
    QIcon tempIcon ;
    tempIcon.addPixmap(pixmapImage);
    QListWidgetItem *tempItem = new QListWidgetItem(tempIcon,text);

    ui->galleryListWidget->addItem(tempItem);

}

void visualogyxApp::addToGallery(QPixmap pixmapImage  )
{
    QIcon tempIcon ;
    tempIcon.addPixmap(pixmapImage);
    QListWidgetItem *tempItem ;
    tempItem->setIcon(tempIcon);

    ui->galleryListWidget->addItem(tempItem);

}

void visualogyxApp::addToGallery(QString imagePath )
{
    QPixmap tempPixmap ;
    tempPixmap.load(imagePath);

    QIcon tempIcon ;
    tempIcon.addPixmap(tempPixmap);

    QListWidgetItem* tempItem ;
    tempItem->setIcon(tempIcon);

    ui->galleryListWidget->addItem(tempItem);

}

//----------------------------------------------------------------------------------------------------

