#ifndef VISUALOGYXAPP_H
#define VISUALOGYXAPP_H

#include <QDialog>

namespace Ui {
class visualogyxApp;
}

class visualogyxApp : public QDialog
{
    Q_OBJECT

public:
    explicit visualogyxApp(QWidget *parent = 0);
    ~visualogyxApp();

    void setWidgetSize(QSize size);


    void addToGallery(QPixmap pixmapImage, QString text);
    void addToGallery(QPixmap pixmapImage);
    void addToGallery(QString imagePath);


private slots:


    void on_loginButton_clicked();

    void on_galleryButton_clicked();

    void on_toggleCameraButton_toggled(bool checked);

    void on_captureButton_clicked();

private:
    Ui::visualogyxApp *ui;
};

#endif // VISUALOGYXAPP_H
