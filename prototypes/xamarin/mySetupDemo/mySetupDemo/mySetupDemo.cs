﻿using System;

using Xamarin.Forms;

namespace mySetupDemo
{
	public class App : Application
	{
		public App ()
		{
			// adding button
			Button btn = new Button();
			btn.Text = "Click me";


			// The root page of your application
			MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							XAlign = TextAlignment.Center,
							Text = "Welcome to Xamarin Forms!"
						},
						btn
					}
				}
			};

			btn.Clicked += (sender, e) => {

				btn.Text = "Clicked me";
			};

		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

