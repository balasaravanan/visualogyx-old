class Student < ActiveRecord::Base
    scope :sorted, lambda { order("students.created_at DESC")};
    scope :search, lambda {|query| where(["first_name LIKE ?", "%#{query}%"])}
end
