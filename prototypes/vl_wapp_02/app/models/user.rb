class User < ActiveRecord::Base
# By default the table name will be users
# To change the table to use in this model use
#self.table_name = "admin_users"

    #Short form
    #attr_accesor: first_name
    
    # Long form 
    #def first_name
    #    @first_name
    #end
    
    #def first_name=(value)
    #   @first_name = value
    #end
    
    #Both long and short form not required to data base models
    
    # scope :visible, lambda {where (:visible => true )}
    # def scope
    #     where(:visible => true)
    # end
    
    scope :search_first_name, lambda {|query|
        where(["first_name LIKE ?", "%#{query}%"])
    }
    
end
