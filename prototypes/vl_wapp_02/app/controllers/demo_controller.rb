class DemoController < ApplicationController

  layout false
  
  #action name is index
  def index
  end
  
  #action name is action_two
  def action_two
    render('demo/hello')
    #render(:template=> 'demo/hello') # this will also works
  end
  
  #action name is hello
  def hello
  end
  
  #action name ins instance_action
  def instance_action
    @array = [1,2,3,4,5]
  end
  
  #action parameters demo
  def params_action
    @id = params[:id]
    @page = params[:page]
    #the below also works
    #@id = params['id']
    #@page = params['page']
  end
end
