class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|

      t.column "first_name", :string, :limit => 25
      #t.type "first_name", options
      
      t.string "last_name", :limit=> 50
      t.string "email", :default => "", :null => false
      t.string "password", :limit => 40
      
      t.timestamps
      
    end
  end
end
