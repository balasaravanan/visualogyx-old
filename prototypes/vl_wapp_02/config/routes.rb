Rails.application.routes.draw do


  get 'inspections/index'

  get 'inspections/show'

  get 'inspections/new'

  get 'inspections/edit'

  get 'inspections/delete'

  get 'students/index'

  get 'students/show'

  get 'students/new'

  get 'students/edit'

  get 'students/delete'

  get 'students_controller/index'

  get 'students_controller/show'

  get 'students_controller/new'

  get 'students_controller/edit'

  get 'students_controller/delete'

  get 'subjects/index'

  get 'subjects/show'

  get 'subjects/new'

  get 'subjects/edit'

  get 'subjects/delete'

  get 'demo/index'
  get 'demo/hello'
  get 'demo/action_two'
  get 'demo/instance_action'
  get 'demo/params_action'

  root "demo#index"
  
  
end
